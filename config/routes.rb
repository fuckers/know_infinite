RailsDevise::Application.routes.draw do
  resources :subscriptions

  resources :nugget_types

  resources :vimeo_embeds

  resources :youtube_embeds

  root :to => "home#index"
  devise_for :users, :controllers => {:registrations => "registrations"}
  match "courses/browse", :to=> "courses#browse", :via=>:get
  match "courses/teach", :to=> "courses#teach", :via=>:get
  match "/users/show", :to=> "home#show", :via=>:get
  match "/users/update",:to=> "users#update",:via=>:post
  #match "/subscriptions/user",:to=> "subscriptions#user_subscriptions",:via=>:get
  resources :users
  resources :nuggets
  resources :courses do
    member do
      get 'view'
      get 'subscribe'
    end
  end

end