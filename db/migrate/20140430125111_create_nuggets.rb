class CreateNuggets < ActiveRecord::Migration
  def change
    create_table :nuggets do |t|
      t.string :name
      t.text :description
      t.integer :course_id
      t.integer :user_id
      t.integer :position

      t.timestamps
    end
  end
end
