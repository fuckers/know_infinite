class CreateYoutubeEmbeds < ActiveRecord::Migration
  def change
    create_table :youtube_embeds do |t|
      t.string :youtube_id
      t.integer :nugget_id

      t.timestamps
    end
  end
end
