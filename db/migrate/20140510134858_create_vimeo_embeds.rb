class CreateVimeoEmbeds < ActiveRecord::Migration
  def change
    create_table :vimeo_embeds do |t|
      t.string :vimeo_id
      t.integer :nugget_id

      t.timestamps
    end
  end
end
