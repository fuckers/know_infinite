require 'test_helper'

class YoutubeEmbedsControllerTest < ActionController::TestCase
  setup do
    @youtube_embed = youtube_embeds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:youtube_embeds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create youtube_embed" do
    assert_difference('YoutubeEmbed.count') do
      post :create, youtube_embed: { nugget_id: @youtube_embed.nugget_id, youtube_id: @youtube_embed.youtube_id }
    end

    assert_redirected_to youtube_embed_path(assigns(:youtube_embed))
  end

  test "should show youtube_embed" do
    get :show, id: @youtube_embed
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @youtube_embed
    assert_response :success
  end

  test "should update youtube_embed" do
    patch :update, id: @youtube_embed, youtube_embed: { nugget_id: @youtube_embed.nugget_id, youtube_id: @youtube_embed.youtube_id }
    assert_redirected_to youtube_embed_path(assigns(:youtube_embed))
  end

  test "should destroy youtube_embed" do
    assert_difference('YoutubeEmbed.count', -1) do
      delete :destroy, id: @youtube_embed
    end

    assert_redirected_to youtube_embeds_path
  end
end
