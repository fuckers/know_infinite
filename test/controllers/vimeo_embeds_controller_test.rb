require 'test_helper'

class VimeoEmbedsControllerTest < ActionController::TestCase
  setup do
    @vimeo_embed = vimeo_embeds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vimeo_embeds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vimeo_embed" do
    assert_difference('VimeoEmbed.count') do
      post :create, vimeo_embed: { nugget_id: @vimeo_embed.nugget_id, vimeo_id: @vimeo_embed.vimeo_id }
    end

    assert_redirected_to vimeo_embed_path(assigns(:vimeo_embed))
  end

  test "should show vimeo_embed" do
    get :show, id: @vimeo_embed
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vimeo_embed
    assert_response :success
  end

  test "should update vimeo_embed" do
    patch :update, id: @vimeo_embed, vimeo_embed: { nugget_id: @vimeo_embed.nugget_id, vimeo_id: @vimeo_embed.vimeo_id }
    assert_redirected_to vimeo_embed_path(assigns(:vimeo_embed))
  end

  test "should destroy vimeo_embed" do
    assert_difference('VimeoEmbed.count', -1) do
      delete :destroy, id: @vimeo_embed
    end

    assert_redirected_to vimeo_embeds_path
  end
end
