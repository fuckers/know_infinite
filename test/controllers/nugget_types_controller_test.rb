require 'test_helper'

class NuggetTypesControllerTest < ActionController::TestCase
  setup do
    @nugget_type = nugget_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:nugget_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create nugget_type" do
    assert_difference('NuggetType.count') do
      post :create, nugget_type: { name: @nugget_type.name }
    end

    assert_redirected_to nugget_type_path(assigns(:nugget_type))
  end

  test "should show nugget_type" do
    get :show, id: @nugget_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @nugget_type
    assert_response :success
  end

  test "should update nugget_type" do
    patch :update, id: @nugget_type, nugget_type: { name: @nugget_type.name }
    assert_redirected_to nugget_type_path(assigns(:nugget_type))
  end

  test "should destroy nugget_type" do
    assert_difference('NuggetType.count', -1) do
      delete :destroy, id: @nugget_type
    end

    assert_redirected_to nugget_types_path
  end
end
