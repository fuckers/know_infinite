json.array!(@vimeo_embeds) do |vimeo_embed|
  json.extract! vimeo_embed, :id, :vimeo_id, :nugget_id
  json.url vimeo_embed_url(vimeo_embed, format: :json)
end
