json.array!(@nuggets) do |nugget|
  json.extract! nugget, :id, :name, :description, :course_id, :user_id, :position
  json.url nugget_url(nugget, format: :json)
end
