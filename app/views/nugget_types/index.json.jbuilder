json.array!(@nugget_types) do |nugget_type|
  json.extract! nugget_type, :id, :name
  json.url nugget_type_url(nugget_type, format: :json)
end
