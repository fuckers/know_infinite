json.array!(@youtube_embeds) do |youtube_embed|
  json.extract! youtube_embed, :id, :youtube_id, :nugget_id
  json.url youtube_embed_url(youtube_embed, format: :json)
end
