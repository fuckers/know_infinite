class NuggetTypesController < ApplicationController
  before_action :set_nugget_type, only: [:show, :edit, :update, :destroy]

  # GET /nugget_types
  # GET /nugget_types.json
  def index
    @nugget_types = NuggetType.all
  end

  # GET /nugget_types/1
  # GET /nugget_types/1.json
  def show
  end

  # GET /nugget_types/new
  def new
    @nugget_type = NuggetType.new
  end

  # GET /nugget_types/1/edit
  def edit
  end

  # POST /nugget_types
  # POST /nugget_types.json
  def create
    @nugget_type = NuggetType.new(nugget_type_params)

    respond_to do |format|
      if @nugget_type.save
        format.html { redirect_to @nugget_type, notice: 'Nugget type was successfully created.' }
        format.json { render :show, status: :created, location: @nugget_type }
      else
        format.html { render :new }
        format.json { render json: @nugget_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nugget_types/1
  # PATCH/PUT /nugget_types/1.json
  def update
    respond_to do |format|
      if @nugget_type.update(nugget_type_params)
        format.html { redirect_to @nugget_type, notice: 'Nugget type was successfully updated.' }
        format.json { render :show, status: :ok, location: @nugget_type }
      else
        format.html { render :edit }
        format.json { render json: @nugget_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nugget_types/1
  # DELETE /nugget_types/1.json
  def destroy
    @nugget_type.destroy
    respond_to do |format|
      format.html { redirect_to nugget_types_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nugget_type
      @nugget_type = NuggetType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nugget_type_params
      params.require(:nugget_type).permit(:name)
    end
end
