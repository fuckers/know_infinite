class SubscriptionsController < ApplicationController
  before_action :confirm_logged_in
  before_action :set_subscription, only: [:show, :edit, :update, :destroy]
  before_action :find_user

  # GET /subscriptions
  # GET /subscriptions.json
  def index
    @user_subscriptions = Subscription.where(:user_id=>@user.id).distinct
    @courses = Array.new
    @user_subscriptions.each do |subscription|
      @cur_course_id = subscription.course_id
      @cur_course = Course.find(@cur_course_id)
      @courses.push(@cur_course)
    end
  end

  # GET /subscriptions/1
  # GET /subscriptions/1.json
  
  def show
  end

  #GET /subscriptions/user

  #def user_subscriptions
  #  @user = User.find(current_user.id)
  #  @user_subscriptions = @user.subscriptions.latest_updated
  #end


  # GET /subscriptions/new
  def new
    @subscription = Subscription.new
  end

  # GET /subscriptions/1/edit
  def edit
  end

  # POST /subscriptions
  # POST /subscriptions.json
  def create
    @subscription = Subscription.new(subscription_params)

    respond_to do |format|
      if @subscription.save
        format.html { redirect_to @subscription, notice: 'Subscription was successfully created.' }
        format.json { render :show, status: :created, location: @subscription }
      else
        format.html { render :new }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subscriptions/1
  # PATCH/PUT /subscriptions/1.json
  def update
    respond_to do |format|
      if @subscription.update(subscription_params)
        format.html { redirect_to @subscription, notice: 'Subscription was successfully updated.' }
        format.json { render :show, status: :ok, location: @subscription }
      else
        format.html { render :edit }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subscriptions/1
  # DELETE /subscriptions/1.json
  def destroy
    @subscription.destroy
    respond_to do |format|
      format.html { redirect_to subscriptions_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subscription
      @subscription = Subscription.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subscription_params
      params.require(:subscription).permit(:user_id, :course_id)
    end

    def find_user
      @user = User.find(current_user.id)
    end
end

