class VimeoEmbedsController < ApplicationController
  before_action :set_vimeo_embed, only: [:show, :edit, :update, :destroy]

  # GET /vimeo_embeds
  # GET /vimeo_embeds.json
  def index
    @vimeo_embeds = VimeoEmbed.all
  end

  # GET /vimeo_embeds/1
  # GET /vimeo_embeds/1.json
  def show
  end

  # GET /vimeo_embeds/new
  def new
    @vimeo_embed = VimeoEmbed.new
  end

  # GET /vimeo_embeds/1/edit
  def edit
  end

  # POST /vimeo_embeds
  # POST /vimeo_embeds.json
  def create
    @vimeo_embed = VimeoEmbed.new(vimeo_embed_params)

    respond_to do |format|
      if @vimeo_embed.save
        format.html { redirect_to @vimeo_embed, notice: 'Vimeo embed was successfully created.' }
        format.json { render :show, status: :created, location: @vimeo_embed }
      else
        format.html { render :new }
        format.json { render json: @vimeo_embed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vimeo_embeds/1
  # PATCH/PUT /vimeo_embeds/1.json
  def update
    respond_to do |format|
      if @vimeo_embed.update(vimeo_embed_params)
        format.html { redirect_to @vimeo_embed, notice: 'Vimeo embed was successfully updated.' }
        format.json { render :show, status: :ok, location: @vimeo_embed }
      else
        format.html { render :edit }
        format.json { render json: @vimeo_embed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vimeo_embeds/1
  # DELETE /vimeo_embeds/1.json
  def destroy
    @vimeo_embed.destroy
    respond_to do |format|
      format.html { redirect_to vimeo_embeds_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vimeo_embed
      @vimeo_embed = VimeoEmbed.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vimeo_embed_params
      params.require(:vimeo_embed).permit(:vimeo_id, :nugget_id)
    end
end
