class HomeController < ApplicationController
  def index
    @users = User.all
    #user = User.find(current_user.id)
    #@cur_user = user.name
    if current_user

    	redirect_to("/users/show")
    end
  end
  def show
  	user = User.find(current_user.id)
  	@cur_user = user.name
  	@cur_pic = user.avatar.url(:medium)
  	@cur_pic_thumb = user.avatar.url(:thumb)
  	@cur_id = user.id
  end

  def update
  	@user = User.find(params[:id])
    @user.update_attribute(:avatar, params[:user][:avatar])  	
  end
end

