class CoursesController < ApplicationController
  before_action :confirm_logged_in, except: [:browse, :view, :teach] 
  before_action :set_course, only: [:show, :edit, :update, :destroy, :view, :subscribe]
  before_action :find_user, except: [:browse, :view, :teach]

  def browse
    @courses = Course.latest_updated
    @no_of_courses = @courses.count
  end
  # GET /courses
  # GET /courses.json
  def index
    @courses = @user.courses.latest_updated
  end

  def teach
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
  end

  def view
  end

  # GET /courses/new
  def new
    @course = Course.new
  end

  # GET /courses/1/edit
  def edit
  end

  # POST /courses
  # POST /courses.json
  def create
    @course = Course.new(course_params)
    @course.user_id = current_user.id

    respond_to do |format|
      if @course.save
        format.html { redirect_to @course, notice: 'Course was successfully created.' }
        format.json { render action: 'show', status: :created, location: @course }
      else
        format.html { render action: 'new' }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    respond_to do |format|
      if @course.update(course_params)
        format.html { redirect_to @course, notice: 'Course was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    @course.destroy
    respond_to do |format|
      format.html { redirect_to courses_url }
      format.json { head :no_content }
    end
  end

  def subscribe
    @subscription = Subscription.new
    @subscription.user_id = current_user.id
    @subscription.course_id = @course.id
    if(@subscription.valid?)
      @subscription.save
      redirect_to view_course_url(:id => @course.id), notice: 'You have been subscribed to this course'
    else
      redirect_to view_course_url(:id => @course.id), notice: 'You have already subscribed to this course'

    end
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:name, :description,:avatar)
    end

    def find_user
      @user = User.find(current_user.id)
    end
end
