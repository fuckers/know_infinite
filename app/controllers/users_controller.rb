class UsersController < ApplicationController
  before_filter :authenticate_user!

  def index
    @users = User.all
  end

  def show
    @user = User.find(current_user.id)
  end

  def update
  	@user = current_user
    @user.update_attribute(:avatar, params[:user][:avatar])  
    redirect_to("/users/show/")	
  end

end
