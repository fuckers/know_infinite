class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include PublicActivity::StoreController
  def save_login_state
    if current_user
      redirect_to(:controller => 'sessions', :action => 'home')
      return false
    else
      return true
    end
  end
  protect_from_forgery with: :exception

  private

  def confirm_logged_in
    unless current_user
      flash[:notice] = "Please log in."
      puts "login failed"
      redirect_to(:controller => 'devise/sessions', :action => 'new')
      return false # halts the before_action
    else
      return true
    end
  end

  



  protect_from_forgery with: :exception
end
