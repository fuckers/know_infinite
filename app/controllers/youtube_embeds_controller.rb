class YoutubeEmbedsController < ApplicationController
  before_action :set_youtube_embed, only: [:show, :edit, :update, :destroy]
  before_action :confirm_logged_in

  # GET /youtube_embeds
  # GET /youtube_embeds.json
  def index
    @youtube_embeds = YoutubeEmbed.all
  end

  # GET /youtube_embeds/1
  # GET /youtube_embeds/1.json
  def show
  end

  # GET /youtube_embeds/new
  def new
    @youtube_embed = YoutubeEmbed.new
  end

  # GET /youtube_embeds/1/edit
  def edit
  end

  # POST /youtube_embeds
  # POST /youtube_embeds.json
  def create
    @youtube_embed = YoutubeEmbed.new(youtube_embed_params)

    respond_to do |format|
      if @youtube_embed.save
        format.html { redirect_to @youtube_embed, notice: 'Youtube embed was successfully created.' }
        format.json { render :show, status: :created, location: @youtube_embed }
      else
        format.html { render :new }
        format.json { render json: @youtube_embed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /youtube_embeds/1
  # PATCH/PUT /youtube_embeds/1.json
  def update
    respond_to do |format|
      if @youtube_embed.update(youtube_embed_params)
        format.html { redirect_to @youtube_embed, notice: 'Youtube embed was successfully updated.' }
        format.json { render :show, status: :ok, location: @youtube_embed }
      else
        format.html { render :edit }
        format.json { render json: @youtube_embed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /youtube_embeds/1
  # DELETE /youtube_embeds/1.json
  def destroy
    @youtube_embed.destroy
    respond_to do |format|
      format.html { redirect_to youtube_embeds_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_youtube_embed
      @youtube_embed = YoutubeEmbed.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def youtube_embed_params
      params.require(:youtube_embed).permit(:youtube_id, :nugget_id)
    end

    def set_nugget
      if params[:nugget_id]
        @course = Course.find(params[:nugget_id])
      elsif params[:youtube_embed][:nugget_id]
        @course = Course.find(params[:youtube_embed][:nugget_id])
      else
        puts "Could not find course_id"
      end
    end
end
