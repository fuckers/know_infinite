$ ->
  $('.subscribe-button').click -> 
    r = confirm 'Are you sure you would like to subscribe to this course?'
    if r
    	course_id = $(this).attr 'id'
    	window.location.replace "/courses/"+course_id+"/subscribe" 
