class Course < ActiveRecord::Base
	include PublicActivity::Model
  	tracked
  	tracked owner: Proc.new{ |controller, model| controller.current_user }
	belongs_to :user, :foreign_key => "user_id"
	has_many :nuggets
	has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
 	validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

	#named scopes
	scope :latest_updated, lambda { order("courses.updated_at DESC")}
end