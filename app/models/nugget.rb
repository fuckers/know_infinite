class Nugget < ActiveRecord::Base
	include PublicActivity::Model
  	tracked
	tracked owner: Proc.new{ |controller, model| controller.current_user }
	belongs_to :course
	acts_as_list :scope => :course

	#named scopes
	scope :sorted, lambda { order("nuggets.position ASC") }
end
